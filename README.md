# **移动端购物街**

## 1. 项目地址： https://github.com/codeHouse9/shopMail

## 2. 演示地址：http://codehouse9.gitee.io/shopping-mall

## 3. 项目说明：

### 3.1本项目是根据coderwhy老师的Vue教程完成

### 3.2 本项目接口的有效时间是从2020年9月至 2021年9月有效

## 4. 效果图：

<img src="https://s1.ax1x.com/2020/10/12/02zW1x.png" alt="index" style="zoom:50%;" />

<img src="https://s1.ax1x.com/2020/10/12/0RSM59.png" alt="cate" style="zoom:50%;" />

<img src="https://s1.ax1x.com/2020/10/12/0RSKUJ.png" alt="cat" style="zoom:50%;" />

<img src="https://s1.ax1x.com/2020/10/12/0RSlCR.png" alt="datalist" style="zoom:50%;" />

<img src="https://s1.ax1x.com/2020/10/12/0RSuE4.png" alt="me" style="zoom:50%;" />